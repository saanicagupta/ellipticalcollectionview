
//
//  EllipseCell.swift
//  EllipseCollectionVIew
//
//  Created by Saanica Gupta 14/08/18.
//  Copyright © 2018 Saanica Gupta All rights reserved.

import UIKit


class EllipseTwoLayout: UICollectionViewLayout {
  //MARK: Instance variables
  var totalPoints:Int = 1
  var xOffset:Float!
  var yOffset:Float!
  var xCord:Float!
  var yCord:Float!
  
//    override init(withXCoordinateConstant xCoordConst:Float?, andWithYCoordinateConstant yCoordConst:Float?) {
//
//    }
    
  //MARK: Custom methods
  func sind(degrees: Float) -> Float {
    return sin(degrees * .pi / 180.0)
  }
  
  func cosd(degrees: Float) -> Float {
    return cos(degrees * .pi / 180.0)
  }
  
  //MARK: Lifecycle methods
  override func prepare() {
    xOffset = Float((collectionView?.frame.width)!)/2 - 10
    yOffset = Float((collectionView?.frame.height)!)/2 - 10
  }
  
  override var collectionViewContentSize: CGSize {
    return CGSize(width: ((collectionView?.frame.width)!),
                  height: (collectionView?.frame.height)!)
  }
  
  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    var attributes=[UICollectionViewLayoutAttributes]()
    for i in 0 ..< ((collectionView?.numberOfItems(inSection: 0))!) {
      let indexPath = IndexPath(item: i, section: 0)
      totalPoints=collectionView!.numberOfItems(inSection: 0)
      attributes.append(layoutAttributesForItem(at: indexPath)!)
    }
    return attributes
  }
  
  override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    let layoutAttributes=UICollectionViewLayoutAttributes(forCellWith: indexPath)
    let cal:Float=360.0/Float(totalPoints)
    let theta:Float=Float(cal * Float(indexPath.row))
    let xCord:Float = cosd(degrees: theta - 90)
    let yCord:Float = sind(degrees: theta - 90)
    //Review: We have created three different CollectionViewLayouts which precisely are doing the same thing...i guess we are doing this just to satisfy the constants used below...these constants are different for all the different collection view's... i have written a commented initializer above...please add the necessary params to the initializer and have global variables to save the values of those constants....we don't need these different layout classes for this...let us create a common layout class and use it for all the collection views...also, i can see we have used three different collection view cells which are precisely the same with just different background colors...let's try and create just one collection view cell for all the three collection views...there is a way to create custom collection view cell using xib and registering that cell with any collection view we want to...try and do that...if you are stuck anywhere with this then let me know...
    layoutAttributes.frame=CGRect(x: Int(xOffset + xCord * 100.0),
                                  y: Int(yOffset + yCord * 150.0),
                                  width: 20,
                                  height: 20)
    return layoutAttributes
  }
  
  static func angleRequired(xAngle: Float, yAngle: Float) -> (Float,Float) {
    let x = xAngle
    let y = yAngle
    return (x,y)
    
    
  }
}


