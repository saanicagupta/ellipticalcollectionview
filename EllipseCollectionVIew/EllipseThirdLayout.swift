//
//  EllipseThirdLayout.swift
//  EllipseCollectionVIew
//
//  Created by Saanica Gupta on 14/08/18.
//  Copyright © 2018 Saanica Gupta. All rights reserved.
//

import UIKit

class EllipseThirdLayout: UICollectionViewLayout {
  var totalPoints:Int = 1
  var xOffset:Float!
  var yOffset:Float!
  var xCord:Float!
  var yCord:Float!
  
  //MARK: Custom methods
  func sind(degrees: Float) -> Float {
    return sin(degrees * .pi / 180.0)
  }
  
  func cosd(degrees: Float) -> Float {
    return cos(degrees * .pi / 180.0)
  }
  
  //MARK: Lifecycle methods
  override func prepare() {
    xOffset = Float((collectionView?.frame.width)!)/2 - 10
    yOffset = Float((collectionView?.frame.height)!)/2 - 10
  }
  
  override var collectionViewContentSize: CGSize {
    return CGSize(width: ((collectionView?.frame.width)!),
                  height: (collectionView?.frame.height)!)
  }
  
  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    var attributes=[UICollectionViewLayoutAttributes]()
    for i in 0 ..< ((collectionView?.numberOfItems(inSection: 0))!) {
      let indexPath = IndexPath(item: i, section: 0)
      totalPoints=collectionView!.numberOfItems(inSection: 0)
      attributes.append(layoutAttributesForItem(at: indexPath)!)
    }
    return attributes
  }
  
  override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    let layoutAttributes=UICollectionViewLayoutAttributes(forCellWith: indexPath)
    let cal:Float=360.0/Float(totalPoints)
    let theta:Float=Float(cal * Float(indexPath.row))
    let xCord:Float = cosd(degrees: theta - 90)
    let yCord:Float = sind(degrees: theta - 90)
    layoutAttributes.frame=CGRect(x: Int(xOffset + xCord * 160.0),
                                  y: Int(yOffset + yCord * 240.0),
                                  width: 20,
                                  height: 20)
    return layoutAttributes
  }
  
  static func angleRequired(xAngle: Float, yAngle: Float) -> (Float,Float) {
    let x = xAngle
    let y = yAngle
    return (x,y)
    
    
  }

}
