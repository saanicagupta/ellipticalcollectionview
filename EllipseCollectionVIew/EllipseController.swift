//
//  EllipseController.swift
//  EllipseCollectionVIew
//
//  Created by Saanica Gupta 14/08/18.
//  Copyright © 2018 Saanica Gupta All rights reserved.


import UIKit

class EllipseController: UIViewController {
  
  //MARK:- Instance variables
  
  //Collection View Data Source Variables
  var cells : [Int] = []
  var cellsTwo : [Int] = []
  var cellsThree : [Int] = []
  
  var newView : UIView!
  var numberOfVIews : [Int] = [] //Number of Mimic View (Black OverLays on Cell)
  lazy var panGesture = UIPanGestureRecognizer()
  var currentSelectedCollectionView: UICollectionView?
  var currentSelectedIndexOne: IndexPath?
  var currentSelectedIndexTwo: IndexPath?
  var currentSelectedIndexThree: IndexPath?
  var locationOfBeganPan: CGPoint!
  var locationOfEndPan: CGPoint!
  
  //MARK: Storyboard Outlets
  @IBOutlet weak var ellipseCollectionView: UICollectionView!
  @IBOutlet weak var collectionViewTwo: UICollectionView!
  @IBOutlet weak var collectionViewThird: UICollectionView!
  @IBOutlet weak var plusOne: UIButton!
  @IBOutlet weak var plusTwo: UIButton!
  @IBOutlet weak var plusThree: UIButton!
  @IBOutlet weak var deleteOne: UIButton!
  
  //MARK:- LifeCycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    //Review: Create a separate function setupUI() and do all the initial ui setup there...call that function from here...the view controller lifecycle functions should not have any unnecessary code....
    panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
  }
  
  
  //MARK:- IBOutlets Actions
  @IBAction func plusBtn(_ sender: Any) {
    cells.append(0)
    self.ellipseCollectionView.insertItems(at: [IndexPath(item:0, section:0)])
  }
  
  @IBAction func buttonTwo(_ sender: Any) {
    cellsTwo.append(0)
    self.collectionViewTwo.insertItems(at: [IndexPath(item:0, section:0)])
  }
  
  @IBAction func plusButtonThree(_ sender: Any ) {
    cellsThree.append(0)
    self.collectionViewThird.insertItems(at: [IndexPath(item:0, section:0)])
  }
  
  @IBAction func minusBtn(_ sender: Any) {
    if cells.count > 0 {
      cells.removeLast()
      self.ellipseCollectionView.deleteItems(at: [IndexPath(item:0, section:0)])
    }
  }
  
  @IBAction func handlePan(recognizer:UIPanGestureRecognizer) {
    let translation = recognizer.translation(in: self.view)
    if recognizer.state == .began {
      if  let recognizerView = recognizer.view {
        locationOfBeganPan = recognizerView.center      //To compare beginning and end frames to animate back to same collection view
      }
    } else if recognizer.state == .changed {
      if let view = recognizer.view {
        view.center = CGPoint(x:view.center.x + translation.x,
                              y:view.center.y + translation.y)
      }
      recognizer.setTranslation(CGPoint.zero, in: self.view)
    }
    else if recognizer.state == .ended {
      if let endedRecognizerView = recognizer.view {
        findDropCollectionViewAndAddCell(frame: endedRecognizerView.frame)
        locationOfEndPan = endedRecognizerView.center   //To compare beginning and end frames to animate back to same collection view
      }
    } else {
      print("error in pan")
    }
  }
  
  func mimicViewDroppedInSameCollectionView() {
    removeSelectedIndex()
    UIView.animate(withDuration: 0.3, animations: {
      self.newView.center = self.locationOfBeganPan
      self.view.layoutIfNeeded()
    }) { (true) in
      self.reloadAllCollectionViews()
      self.newView.removeFromSuperview()
    }
  }
  
  func removeSelectedIndex() {
    currentSelectedIndexOne = nil
    currentSelectedIndexTwo = nil
    currentSelectedIndexThree = nil
  }
  
  func reloadAllCollectionViews(){
    ellipseCollectionView.reloadData()
    collectionViewTwo.reloadData()
    collectionViewThird.reloadData()
  }
  
  func handleRemovalOfCell() {
    if currentSelectedCollectionView == ellipseCollectionView {
      if let index = currentSelectedIndexOne?.row {
        if (ellipseCollectionView.cellForItem(at: IndexPath(item: index, section: 0)) != nil) { //checking if cell exists
          cells.remove(at: index)
          currentSelectedIndexOne = nil
        }
        ellipseCollectionView.reloadData()
      }
    } else if currentSelectedCollectionView == collectionViewTwo {
      if let index = currentSelectedIndexTwo?.row {
        if (collectionViewTwo.cellForItem(at: IndexPath(item: index, section: 0)) != nil) {  //checking if cell exists
          cellsTwo.remove(at: index)
          currentSelectedIndexTwo = nil
        }
        collectionViewTwo.reloadData()
        
      }
    } else if currentSelectedCollectionView == collectionViewThird {
      if let index = currentSelectedIndexThree?.row {
        if (collectionViewThird.cellForItem(at: IndexPath(item: index, section: 0)) != nil) {  //checking if cell exists
          cellsThree.remove(at: index)
          currentSelectedIndexThree = nil
        }
        collectionViewThird.reloadData()
      }
    }
  }
  
  
  func addCustomView(x: CGFloat, y: CGFloat, framePoint: CGPoint) {
    if ((numberOfVIews.count == 0) && (numberOfVIews.count < 2)) {
      newView = UIView()
      newView.addGestureRecognizer(panGesture)            //adding pan gesture to mimic view
      newView.frame = CGRect.init(x: x, y: y, width: 20, height: 20)
      newView.center = framePoint
      mimicViewColorAndBorder()                         //adding colour to mimic view based on collection view selected
      self.view.addSubview(newView)
    } else if (numberOfVIews.count >= 1) {
      newView.removeFromSuperview()
      newView = UIView()
      newView.addGestureRecognizer(panGesture)         //adding pan gesture to mimic view
      newView.frame = CGRect.init(x: x, y: y, width: 20, height: 20)
      newView.center = framePoint
      mimicViewColorAndBorder()                         //adding colour to mimic view based on collection view selected
      self.view.addSubview(newView)
    }
  }
  
  
  func mimicViewColorAndBorder() {
    if (currentSelectedCollectionView == ellipseCollectionView) {
      newView.backgroundColor = UIColor(red: 255/255, green: 106/255, blue: 97/255, alpha: 1)
    } else if (currentSelectedCollectionView == collectionViewTwo) {
      newView.backgroundColor = UIColor.magenta
    } else if (currentSelectedCollectionView == collectionViewThird) {
      newView.backgroundColor = UIColor(red: 146/255, green: 144/255, blue: 0/255, alpha: 1)
    } else {
      print("Mimic view not given any colour and border -> no collection View selected")
    }
    newView.layer.borderColor = UIColor.black.cgColor
    newView.layer.borderWidth = 2
  }
  
  func findDropCollectionViewAndAddCell(frame: CGRect) {
    if frame.intersects(ellipseCollectionView.frame) { // Adds in inner Collection View
      if currentSelectedCollectionView == ellipseCollectionView {
        mimicViewDroppedInSameCollectionView()
      } else {
        newView.removeFromSuperview()
        plusOne.sendActions(for: .touchUpInside)
      }
    } else if frame.intersects(collectionViewTwo.frame) { // Adds in middle Collection View
      if currentSelectedCollectionView == collectionViewTwo {
        mimicViewDroppedInSameCollectionView()
      } else {
        newView.removeFromSuperview()
        plusTwo.sendActions(for: .touchUpInside)
      }
    } else if frame.intersects(collectionViewThird.frame) { // Adds in Outer Collection View
      if currentSelectedCollectionView == collectionViewThird {
        mimicViewDroppedInSameCollectionView()
      } else {
        newView.removeFromSuperview()
        plusThree.sendActions(for: .touchUpInside)
      }
    } else {
      print("view frame not intersected with any of the collection view")
    }
    handleRemovalOfCell()
  }
}

//MARK: Extensions
extension EllipseController: UICollectionViewDelegate, UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView,
                      numberOfItemsInSection section: Int) -> Int {
    if (collectionView == ellipseCollectionView) {
      return cells.count
    } else if (collectionView == collectionViewTwo) {
      return cellsTwo.count
    } else if (collectionView == collectionViewThird) {
      return cellsThree.count
    } else {
      return 0
    }
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if (collectionView == ellipseCollectionView) {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EllipseCell",
                                                    for: indexPath) as! EllipseCell
      cell.backgroundColor = UIColor(red: 255/255, green: 106/255, blue: 97/255, alpha: 1)
      if ((currentSelectedIndexOne != nil) && (currentSelectedIndexOne == indexPath)) {
        cell.backgroundColor = UIColor.clear
      } else {
        cell.backgroundColor = UIColor(red: 255/255, green: 106/255, blue: 97/255, alpha: 1)
      }
      return cell
    } else if (collectionView == collectionViewTwo) {
      let cell = collectionViewTwo.dequeueReusableCell(withReuseIdentifier: "elipseCellTwo", for: indexPath) as! MiddlellipseCollectionViewCell
      cell.backgroundColor = UIColor.magenta
      if ((currentSelectedIndexTwo != nil) && (currentSelectedIndexTwo == indexPath)) {
        cell.backgroundColor = UIColor.clear
      } else {
        cell.backgroundColor = UIColor.magenta
      }
      return cell
    } else {
      let cell = collectionViewThird.dequeueReusableCell(withReuseIdentifier: "ellipseCellThird", for: indexPath) as! OuterEllipseCollectionViewCell
      cell.backgroundColor = UIColor(red: 146/255, green: 144/255, blue: 0/255, alpha: 1)
      if ((currentSelectedIndexThree != nil) && (currentSelectedIndexThree == indexPath)) {
        cell.backgroundColor = UIColor.clear
      } else {
        cell.backgroundColor = UIColor(red: 146/255, green: 144/255, blue: 0/255, alpha: 1)
      }
      return cell
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    removeSelectedIndex()
    if collectionView == ellipseCollectionView {
      currentSelectedIndexOne = indexPath
      currentSelectedCollectionView = ellipseCollectionView
      if (cells.count > 0) {
        let innerEllipseCell = collectionView.cellForItem(at: indexPath)
        let testFrame = collectionView.convert((innerEllipseCell?.center)!, to: collectionView.superview)
        addCustomView(x: testFrame.x, y: testFrame.y, framePoint: testFrame)
        numberOfVIews.append(0) //View is added (check)
        reloadAllCollectionViews()
      }
    } else if (collectionView == collectionViewTwo) {
      currentSelectedIndexTwo = indexPath
      currentSelectedCollectionView = collectionViewTwo
      if (cellsTwo.count > 0) {
        let middleEllipseCell = collectionView.cellForItem(at: indexPath)
        let middleCellFrameWRTToitsSuperview = collectionView.convert((middleEllipseCell?.center)!, to: collectionView.superview)
        addCustomView(x: middleCellFrameWRTToitsSuperview.x, y: middleCellFrameWRTToitsSuperview.y, framePoint: middleCellFrameWRTToitsSuperview)
        numberOfVIews.append(0)
        reloadAllCollectionViews()
      }
    } else if (collectionView == collectionViewThird) {
      currentSelectedIndexThree = indexPath
      currentSelectedCollectionView = collectionViewThird
      if (cellsThree.count > 0) {
        let outerEllipseCell = collectionView.cellForItem(at: indexPath)
        let outerCellFrameWRTToitsSuperview = collectionView.convert((outerEllipseCell?.center)!, to: collectionView.superview)
        addCustomView(x: outerCellFrameWRTToitsSuperview.x, y: outerCellFrameWRTToitsSuperview.y, framePoint: outerCellFrameWRTToitsSuperview)
        numberOfVIews.append(0)
        reloadAllCollectionViews()
      }
    } else {
      print("No collection view cell selected")
    }
  }
}


