//
//  EllipseCell.swift
//  EllipseCollectionVIew
//
//  Created by Saanica Gupta 14/08/18.
//  Copyright © 2018 Saanica Gupta All rights reserved.
//

import UIKit


class EllipseLayout: UICollectionViewLayout {
  //MARK: Instance variables
  var totalPoints:Int = 1
  var xOffset:Float!
  var yOffset:Float!
  var xCord:Float!
  var yCord:Float!
  
  //MARK: Custom methods
  func sind(degrees: Float) -> Float {
    return sin(degrees * .pi / 180.0)
  }
  
  func cosd(degrees: Float) -> Float {
    return cos(degrees * .pi / 180.0)
  }
  
  //MARK: Lifecycle methods
  override func prepare() {
    //Review: Never use any forced unwrapping... we will never use "!" other than for the not operator...
    //Review: Float((collectionView?.frame.width)!)/2 - 10 : There is too much happening in this statement...we are determining the width firstly, then we are calculating the half of it...then subtracting some random value i.e. 10 from it...Please split all of this in separate lines by creating variables such as 1. collectionViewWidth 2. halfOfCollectionViewWidth 3. adjustmentConstant....then xOffset = halfOfCollectionViewWidth - adjustmentConstant....so that when someone else reads the code he/she is able to clearly understand what is happening here...
    xOffset = Float((collectionView?.frame.width)!)/2 - 10
    yOffset = Float((collectionView?.frame.height)!)/2 - 10
  }
  
  override var collectionViewContentSize: CGSize {
    return CGSize(width: ((collectionView?.frame.width)!),
                  height: (collectionView?.frame.height)!)
  }
  
  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    var attributes=[UICollectionViewLayoutAttributes]()
    //Review: No forced unwrapping...please use if let's or guard statements...
    for i in 0 ..< ((collectionView?.numberOfItems(inSection: 0))!) {
      let indexPath = IndexPath(item: i, section: 0)
      totalPoints=collectionView!.numberOfItems(inSection: 0)
      attributes.append(layoutAttributesForItem(at: indexPath)!)
    }
    return attributes
  }
  
  override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    let layoutAttributes=UICollectionViewLayoutAttributes(forCellWith: indexPath)
    let cal:Float=360.0/Float(totalPoints)
    let theta:Float=Float(cal * Float(indexPath.row))
    let xCord:Float = cosd(degrees: theta - 90)
    let yCord:Float = sind(degrees: theta - 90)
    layoutAttributes.frame=CGRect(x: Int(xOffset + xCord * 40.0),
                                  y: Int(yOffset + yCord * 60.0),
                                  width: 20,
                                  height: 20)
    return layoutAttributes
  }
  
  static func angleRequired(xAngle: Float, yAngle: Float) -> (Float,Float) {
    let x = xAngle
    let y = yAngle
    return (x,y)
    
    
  }
}

